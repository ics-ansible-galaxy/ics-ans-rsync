# ics-ans-rsync

Ansible playbook to configure zfs filesystem and rsync daemon for backup purposes.

Will set the rsync_server_share list automatically by itself.

Through:
```yaml

rsync_module_backup_mounted_folder: "{{zfs_poo}}/{{ ansible_hostname }}"
rsync_server_share:
  - name: "{{ ansible_hostname }}"
    path: "{{ rsync_module_backup_mounted_folder }}"
    hosts_allow: "{{ansible_fqdn }}"

```

Only needed variables for the inital setup are these

```yaml

rsync_run_server: true
zfs_pool_devices:
  - /dev/sda
  - /dev/sdb
zfs_pool_mode: raid0
zfs_pool_name: backup
zfs_pool_vdev: 1
```
Rest are taken from the client side through the inventory

## License

BSD 2-clause
