def inventory_to_hostvars(hosts, hostvars):
    """
    This function returns a list of variables such as hostname,host ip from the inventory hostvars
    """
    result = []

    for host in hosts:

        name = hostvars[host].get("inventory_hostname_short")
        fqdn = hostvars[host].get("inventory_hostname")
        zfs_pool = hostvars[host].get("rsync_module_backup_pool")
        path = zfs_pool + "/" + name
        options = {}

        if hostvars[host].get("rsync_module_backup_excludes"):
            options.update(
                {"excludes": hostvars[host].get("rsync_module_backup_excludes")}
            )

        if hostvars[host].get("rsync_module_backup_includes"):
            options.update(
                {"includes": hostvars[host].get("rsync_module_backup_includes")}
            )

        if hostvars[host].get("rsync_module_backup_exclude_compress"):
            options.update(
                {
                    "exclude_compress": hostvars[host].get(
                        "rsync_module_backup_exclude_compress"
                    )
                }
            )

        if hostvars[host].get("rsync_module_backup_timeout"):
            options.update(
                {"timeout": hostvars[host].get("rsync_module_backup_timeout")}
            )

        if hostvars[host].get("rsync_module_backup_comment"):
            options.update(
                {"comment": hostvars[host].get("rsync_module_backup_comment")}
            )

        if options:
            result.append(
                {"name": name, "path": path, "hosts_allow": fqdn, "options": options}
            )
        else:
            result.append({"name": name, "path": path, "hosts_allow": fqdn})
    return result


class FilterModule(object):
    def filters(self):
        return {"inventory_to_hostvars": inventory_to_hostvars}
